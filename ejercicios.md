# Protocolos para la transmisión de audio y video en Internet
# Práctica 4. Análisis de RTP

**Nota:** Esta práctica se puede entregar para su evaluación como parte de
la nota de prácticas, pudiendo obtener el estudiante hasta un 0.7 puntos. Para
las instrucciones de entrega, mira al final del documento. Para la evaluación
de esta entrega se valorará el correcto funcionamiento de lo que se pide y el
seguimiento de la guía de estilo de Python.

**Conocimientos previos necesarios:**

* Nociones de uso de wireshark (pero repasaremos todo lo que haga falta)
* Nociones de SIP y RTP (las vistas en clase de teoría)

**Tiempo estimado:** 6 horas

**Repositorio plantilla:** https://gitlab.eif.urjc.es/ptavi/2023-2024/p4-rtp

**Fecha de entrega parte individual:** 24 de octubre de 2023, 23:59 (hasta ejercicio 9, incluido)

**Fecha de entrega parte interoperación:** 27 de octubre de 2023, 23:59 (ejercicio 10)

## Introducción

El Protocolo de Transmisión en Tiempo Real (RTP) es un protocolo de nivel de aplicación, utilizado para transmitir datos en tiempo real sobre el protocolo UDP. . Fue desarrollado originalmente para aplicaciones de audio y video en tiempo real, pero también se utiliza en otras aplicaciones que requieren transmisiones en tiempo real, como la transmisión de datos de sensores en redes IoT, o en juegos distribuidos. RTP estaba descrito originalmente en la [RFC 1889](https://datatracker.ietf.org/doc/html/rfc1889), que fue sustituida por [RFC 3550](https://datatracker.ietf.org/doc/html/rfc3550) en 2003.

## Objetivos de la práctica

* Conocer el protocolo RTP.
* Profundizar en el uso de _Wireshark_: análisis, captura y filtrado.

## Ejercicio 1. Creación de repositorio para la práctica

Con el navegador, dirígete al repositorio plantilla de esta práctica y realiza un fork, de manera que consigas tener una copia del repositorio en tu cuenta de GitLab. Clona el repositorio que acabas de crear a local para poder editar los archivos. Trabaja a partir de ahora en ese repositorio, sincronizando los cambios que vayas realizando según los ejercicios que se comentan a continuación (haciendo commit y subiéndolo a tu repositorio en el GitLab de la EIF).

## Ejercicio 2. Prepara la respuesta

Se han capturado una serie de paquetes RTP (archivo `captura.pcap`), que se puede abrir con _Wireshark_ (desde la shell, `wireshark captura.pcap`). Se pide rellenar las cuestiones que se plantean editando este guión, y produciendo luego el fichero `respuesta.md` según se indica más adelante, usando el programa `respuestas.py`, que encontrarás en este repositorio. Ese fichero `respuesta.md` tendrás que subirlo (usando git, como se indica en prácticas anteriores) a tu repositorio de entrega de esta práctica, para su corrección.

Para crear el fichero `respuesta.md`, copia este fichero, en formato Markdown, en un fichero que se llame igual (`ejercicios.md`). Responde a cada una de las preguntas justo a continuación de cada una de ellas. Por último, ejecuta el programa `respuestas.py` para que genere el fichero `respuesta.md`. Realiza commits con las distintas versiones que vayas teniendo de los ficheros `ejercicios.md` y `respuestas.md`, y súbelas tu repositorio de entrega de esta práctica.

Sólo se tomará en cuenta para la corrección el fichero `respuesta.md`, pero también se comprobará que se genera correctamente a partir del fichero `ejercicios.md` con las respuestas correspondientes.

## Ejercicio 3. Análisis general

Vamos a analizar la traza capturada, en general. Si te ayuda, mira las estadísticas generales que aparecen en el menú de `Statistics`. En el apartado de jerarquía de protocolos (`Protocol Hierarchy`) se puede ver, por ejemplo, el número de paquetes y el tráfico correspondiente a los distintos protocolos.

* ¿Cuántos paquetes componen la captura?: 1268
* ¿Cuánto tiempo dura la captura?: 12.810068 segundos
* Aproximadamente, ¿cuántos paquetes se envían por segundo?: 99
* ¿Qué protocolos de nivel de red aparecen en la captura?: Internet Protocol Version 4 (IPv4)
* ¿Qué protocolos de nivel de transporte aparecen en la captura?: User Datagram Portocol (UDP)
* ¿Qué protocolos de nivel de aplicación aparecen en la captura?: Data

Responde a estas preguntas en el fichero `ejercicios.md` en el repositorio clonado a partir de tu repositorio de entrega. Genera a partir de él el fichero `respuesta.md`, como se indicó al principio de este enunciado.  Al terminar el ejercicio haz un commit de ambos ficheros, con el comentario "Ejercicio 3".

## Ejercicio 4. Análisis de un paquete

Elige un número aleatorio entre 1 y 500. Le llamaremos N.

* Número aleatorio elegido (N): 327

Llamemos P al paquete RTP de la traza que tiene como número de secuencia el número N, o si ese no es un paquete RTP, al primer paquete siguiente que sea RTP. Llamamos máquina A la máquina origen del paquete P, y máquina B a la máquina destino del paquete P.

* Dirección IP de la máquina A: 192.168.0.10 
* Dirección IP de la máquina B: 216.234.64.16
* Puerto UDP desde el que se envía el paquete: 49154
* Puerto UDP hacia el que se envía el paquete: 54550
* SSRC del paquete: 0x2a173650 (706164304)
* Tipo de datos en el paquete RTP (según indica el campo correspondiente): ITU-T G-711 PCMU (0)

Sigue respondiendo a estas preguntas en el fichero `ejercicios.md` en el repositorio clonado a partir de tu repositorio de entrega. Genera a partir de él el fichero `respuesta.md`, como se indicó al principio de este enunciado.  Al terminar el ejercicio haz un commit de ambos ficheros, con el comentario "Ejercicio 4".

## Ejercicio 5. Análisis de un flujo (stream)

Usando el menú Telephony | RTP puedes ver cuántos flujos (streams) RTP tenemos en la traza capturada. Verás que hay dos.  Si el primer número de tu DNI o NIE es impar, elige la primera. Si es par, la segunda. Llamaremos a este flujo F, y F2 al otro flujo. 

* ¿Cuál es el primer número de tu DNI / NIE?: 0
* ¿Cuántos paquetes hay en el flujo F?: 626
* ¿El origen del flujo F es la máquina A o B?: B
* ¿Cuál es el puerto UDP de destino del flujo F?: 49154
* ¿Cuántos segundos dura el flujo F?: 12.49

Sigue respondiendo a estas preguntas en el fichero `ejercicios.md` en el repositorio clonado a partir de tu repositorio de entrega. Genera a partir de él el fichero `respuesta.md`, como se indicó al principio de este enunciado.  Al terminar el ejercicio haz un commit de ambos ficheros, con el comentario "Ejercicio 5".

## Ejercicio 6. Métricas del flujo

Todas estas preguntas, para el flujo F.

Llamamos "diferencia" al tiempo (en segundos) entre el momento en que llega un paquete y el momento en que "debería" haber llegado (si hubiera tardado en transmitirse exactamente lo mismo que el paquete anterior).

* ¿Cuál es la diferencia máxima en el flujo?: 21.187000 ms
* ¿Cuál es el jitter medio del flujo?: 0.229065 ms
* ¿Cuántos paquetes del flujo se han perdido?: 0
* ¿Cuánto tiempo (aproximadamente) está el jitter por encima de 0.4 ms?: 0.5 segundos

Sigue respondiendo a estas preguntas en el fichero `ejercicios.md` en el repositorio clonado a partir de tu repositorio de entrega. Genera a partir de él el fichero `respuesta.md`, como se indicó al principio de este enunciado.  Al terminar el ejercicio haz un commit de ambos ficheros, con el comentario "Ejercicio 6".

## Ejercicio 7. Métricas de un paquete RTP

Llamemos PF al paquete de la traza que tiene como número de secuencia el número N que elegiste antes, o si ese no es un paquete de la traza, al primer paquete siguiente que lo sea. Para el paquete PF:

* ¿Cuál es su número de secuencia dentro del flujo RTP?: 18599
* ¿Ha llegado pronto o tarde, con respecto a cuando habría debido llegar?: Pronto
* ¿Qué jitter se ha calculado para ese paquete? 0.219480 ms
* ¿Qué timestamp tiene ese paquete? 1769331723
* ¿Por qué número hexadecimal empieza sus datos de audio? f


Sigue respondiendo a estas preguntas en el fichero `ejercicios.md` en el repositorio clonado a partir de tu repositorio de entrega. Genera a partir de él el fichero `respuesta.md`, como se indicó al principio de este enunciado.  Al terminar el ejercicio haz un commit de ambos ficheros, con el comentario "Ejercicio 7".

## Ejercicio 8. Captura de una traza RTP

Ahora, vamos a realizar la captura de una traza RTP. Para ello, vamos a usar [GStreamer](https://gstreamer.freedesktop.org/), un sistema de tratamiento de sonido bastante completo, que entre otras cosas permite transmitir con paquetes RTP un fichero de audio.

Para generar un flujo RTP, vamos a utilizar el siguiente comando:

```shell
gst-launch-1.0  -v filesrc location = recording.wav ! wavparse ! audioconvert ! rtpL16pay ! udpsink host=127.0.0.1 port=1200
```

Antes de ejecutar el comando, realiza una captura (con Wireshark) en el dispositivo lo (loopback), de forma que tengas en ella todos los paquetes que se transmitan. Justo antes de hacerlo, ejecuta el comando `date` para obtener la fecha y hora de tu máquina. Guarda tu captura en el fichero `rtp.pcap`.

* Salida del comando `date`: lun 16 oct 2023 17:20:47 CEST 
* Número total de paquetes en la captura: 1418 
* Duración total de la captura (en segundos): 11.885910127 

Sigue respondiendo a estas preguntas en el fichero `ejercicios.md` en el repositorio clonado a partir de tu repositorio de entrega. Genera a partir de él el fichero `respuesta.md`, como se indicó al principio de este enunciado.  Al terminar el ejercicio haz un commit de ambos ficheros, y del fichero `rtp.pcap`, con el comentario "Ejercicio 8".

## Ejercicio 9. Análisis de la captura realizada

Abre ahora la captura `rtp.pcal` que has realizado en el ejercico anterior con Wireshark. Localiza el flujo RTP que has capturado y contesta a estas preguntas:

* ¿Cuántos paquetes RTP hay en el flujo?: 450
* ¿Cuál es el SSRC del flujo?: 0x5cc8a508
* ¿En qué momento (en ms) de la traza comienza el flujo? 3.672713281
* ¿Qué número de secuencia tiene el primer paquete del flujo? 32191
* ¿Cuál es el jitter medio del flujo?: 0
* ¿Cuántos paquetes del flujo se han perdido?: 0

Sigue respondiendo a estas preguntas en el fichero `ejercicios.md` en el repositorio clonado a partir de tu repositorio de entrega. Genera a partir de él el fichero `respuesta.md`, como se indicó al principio de este enunciado.  Al terminar el ejercicio haz un commit de ambos ficheros, con el comentario "Ejercicio 9".

## Ejercicio 10 (segundo periodo). Analiza la captura de un compañero

Una vez hayáis entregado la práctica, elige la práctica de un compañero que haya realizado los ejercicios 8 y 9, y utiliza su captura `rtp.pcap`. Copiala a un fichero `rtp2.pcap`, que abrirás más tarde con Wireshark. Responde la siguiente pregunta con el identificador de usuario del repositorio de GitLab que has usado.

* ¿De quién es la captura que estás usando?:

Ahora, compara esa captura con la que hiciste tú en el ejercicio 8. En las siguientes preguntas, utiliza un número positivo si más (o mayor), número negativo si menos (o menor):

* ¿Cuántos paquetes de más o de menos tiene que la tuya? (toda la captura):
* ¿Cuántos paquetes de más o de menos tiene el flujo RTP que el tuyo?:
* ¿Qué diferencia hay entre el número de secuencia del primer paquete de su flujo RTP con respecto al tuyo?:
* En general, ¿que diferencias encuentras entre su flujo RTP y el tuyo?:

Sigue respondiendo a estas preguntas en el fichero `ejercicios.md` en el repositorio clonado a partir de tu repositorio de entrega. Genera a partir de él el fichero `respuesta.md`, como se indicó al principio de este enunciado.  Al terminar el ejercicio haz un commit de ambos ficheros, y del fichero `rtp2.pcap`, con el comentario "Ejercicio 10".


## ¿Qué se valora de esta la práctica?

Valoraremos de esta práctica sólo lo que esté en la rama principal de
tu repositorio, creado de la forma que hemos indicado (como fork del repositorio plantilla que os proporcionamos). Por lo tanto, aségurate de que está en él todo lo que has realizado.

Además, ten en cuenta:

* Se valorará que el fichero de respuestas esté en formato correcto (si no es así, es posible que no se corrija).
* Se valorará que esté el texto de todas las preguntas en el fichero de respuestas, tal y como se indica al principio de este enunciado.
* Se valorará que las respuestas sean fáciles de entender, y estén correctamente relacionadas con las preguntas.
* Se valorará que la captura que se pide tenga exactamente lo que se pide.
* Parte de la corrección será automática, así que asegúrate de que los nombres que utilizas para los archivos son los mismos que indica el enunciado.
